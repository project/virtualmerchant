Virtual Merchant
========

URL: http://drupal.org/project/virtualmerchant

Issues Queue: http://drupal.org/project/issues/virtualmerchant

License
-------

This project is GPL v2 software. See the LICENSE.txt file in this directory for
complete text.


Current Maintainers
-------------------

- Alex Laughnan (https://www.drupal.org/u/laughnan)
- Ryan Szrama (https://www.drupal.org/u/rszrama)
